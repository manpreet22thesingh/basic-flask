import json
import logging
import threading
import time

import requests
from celery.task import periodic_task
from flask import Flask, render_template, request
from pyflock import Attachment, FlockClient, Message, Views

import settings
from flask_celery import make_celery
from get_endpoints import goto_github

app = Flask(__name__)
app.config.from_object(settings)
logger = logging.getLogger(__name__)

bot_token = '8437d8c4-967b-49d7-945e-43797dd3ca4d'
app_id = '786ed21e-2731-4c5c-85d4-49e43cc39ee7'

flock_client = FlockClient(token=bot_token, app_id=app_id)
github_client_id = 'f1d9468ae6292cb78bd1'
flock_base = 'https://web.flock.co/'
github_redirect_url = '/redirect-flock/'
ask_username_str = "please enter your github username"
github_user_end_point = 'https://api.github.com/user'
bye_bot = 'bye bot'
take_git_commands = 'Take git commands'


app_install_str = 'app.install'
app_uninstall_str = 'app.uninstall'
chat_msg_receive = 'chat.receiveMessage'

user_token_dict = {}

class BaseData():
    base_url = ''
    obj = None

    @classmethod
    def get_obj(cls):
        if cls.obj is None:
            cls.obj = BaseData()
        return cls.obj


def get_event_name(request_data):
    event_name_dict = {
        chat_msg_receive: chat_msg_receive,
        app_install_str: app_install_str,
        app_uninstall_str: app_uninstall_str
    }
    name = None
    if request_data['name'] == app_install_str:
        user_token_dict[request_data['userId']] = {
            'flock_token': request_data['token'],
            'is_authenticated': False
        }
        write_file()
        name = 'install'
    elif event_name_dict.get(request_data['name']):
        name = event_name_dict[request_data['name']]
    return name


def send_html(text, user_id):
    time.sleep(2)
    views = Views()
    views.add_flockml("<flockml><a href='{}/authenticate/?user_id={}'>Please authenticate gitbot to access github.</a></flockml>".format(BaseData.get_obj().base_url, user_id))
    attachment = Attachment(title="Test flockml", description="Click here to authenticate", views=views)
    send_as_message = Message(to=user_id, attachments=[attachment])
    res = flock_client.send_chat(send_as_message)


def send_text(text, user_id):
    time.sleep(2)
    simple_message = Message(to=user_id, text=text)
    res = flock_client.send_chat(simple_message)

@app.route('/authenticate/', methods=['POST', 'GET'])
def authenticateGithub():
    url = 'https://github.com/login/oauth/authorize/?scope=user%20public_repo%20repo%20repo:status%20delete_repo20notifications%20read:org%20admin:org%20' \
          'read:public_key%20write:public_key%20admin:public_key&client_id={}&redirect_uri={}?user_id={}'.format(
        github_client_id, BaseData.get_obj().base_url+github_redirect_url, request.args.get('user_id')
    )
    return render_template('response_index.html', title='Home', url=url)


@app.route('/event/', methods=['POST', 'GET'])
def createEvent():
    request_data = json.loads(request.data)
    BaseData.get_obj().base_url = request.url_root
    BaseData.get_obj().base_url = check_and_update_url(BaseData.get_obj().base_url[:-1])
    event_name = get_event_name(request_data)
    if request_data['name'] == app_install_str:
        thread = threading.Thread(target=send_html, args=('Hi', request_data['userId']))
        thread.start()
        return json.dumps({}), 200
    elif event_name == chat_msg_receive:
        if user_token_dict[request_data.get('userId')]['is_authenticated']:
            if user_token_dict[request_data.get('userId')]['next_command'] == ask_username_str:
                request_user_name = requests.get(github_user_end_point+'?access_token={}'.format(user_token_dict[request_data.get('userId')]['github_access_key']))
                user_name = json.loads(request_user_name.content)
                if user_name['login'] != request_data.get('message').get('text'):
                    thread = threading.Thread(target=send_text, args=('Please Enter a valid username', request_data.get('userId')))
                    thread.start()
                else:
                    user_token_dict[request_data.get('userId')]['username'] = request_data.get('message').get('text')
                    thread = threading.Thread(target=send_text,
                                              args=("Hi {} welcome to gitbot. Please Enter your query".format(user_name['login']),
                                                    request_data.get('userId'))
                    )
                    thread.start()
                    user_token_dict[request_data.get('userId')]['next_command'] = take_git_commands
                    write_file()
            elif user_token_dict[request_data.get('userId')]['next_command'] == take_git_commands:
                if request_data.get('message').get('text') == bye_bot:
                    user_token_dict[request_data.get('userId')]['next_command'] = bye_bot
                    thread = threading.Thread(target=send_text, args=('OK Byeee', request_data.get('userId')))
                    thread.start()
                    write_file()
                else:
                    response = goto_github(
                        request_data.get('message').get('text'), user_token_dict[request_data.get('userId')]['username'],
                        request_data.get('userId'), flock_client)
                    user_token_dict[request_data.get('userId')]['next_command'] = take_git_commands
                    write_file()
            return json.dumps({}), 200
        else:
            thread = threading.Thread(target=send_text, args=("Please re-install the GitBot", request_data.get('userId')))
            thread.start()
    else:
        return json.dumps({}), 200


@app.route('/redirect-flock/', methods=['POST', 'GET'])
def redirectFromGithubToFlock():
    if request.args.get('code') and request.args.get('user_id'):
        response = requests.post('https://github.com/login/oauth/access_token', data={
            'client_id': github_client_id, 'code': request.args.get('code'), 'client_secret': 'bf64309d64948a8bbfbd341a1090256b32009f5f'
        })
        if response.content:
            access_token = response.content.split('&')[0]
            if access_token and access_token.split('=')[1]:
                access_token_key = access_token.split('=')[1]
                user_token_dict[request.args.get('user_id')]['is_authenticated'] = True
                user_token_dict[request.args.get('user_id')]['github_access_code'] = request.args.get('code')
                user_token_dict[request.args.get('user_id')]['github_access_key'] = access_token_key
                user_token_dict[request.args.get('user_id')]['next_command'] = ask_username_str
                write_file()

                thread = threading.Thread(target=send_text, args=(ask_username_str, request.args.get('user_id')))
                thread.start()

                return render_template(
                    'response_index.html',
                    url=flock_base
                )

    return render_template(
        '404.html',
        url=flock_base
    )

app.config.update(
    task_serializer='json',
    accept_content=['json'],  # Ignore other content
    result_serializer='json',
    timezone='Europe/Oslo',
    enable_utc=True,
)

fcelery = make_celery(app)


# def hello():
#     logger.info('Adding')

pr_remind_user = {}


def remind_about_pr(pr_remind_user):
    logger.info("gng to call remind about pr now ", json.dumps(pr_remind_user), " bbbb")
    for data in pr_remind_user.values():

        views = Views()
        flock_data="<flockml>"
        for i in data:
                flock_data += "Hey, a new PR, named as {} has been assigned to you,<br/>please click here to <a href='{}'>review</a><br/>".format(i['subject'], i['url'])
        flock_data += "</flockml>"
        views.add_flockml(flock_data)
        attachment = Attachment(description="Reminder", views=views)
        send_as_message = Message(to=data[0]['user_id'], attachments=[attachment])
        res = flock_client.send_chat(send_as_message)


def get_user_id(access_token, user_token_dict):
    for ele in user_token_dict.keys():
        if user_token_dict[ele]['github_access_key'] == access_token:
            return ele
    return None


def write_file():
    f = open('static.txt', 'w')
    f.write(json.dumps(user_token_dict))
    f.close()


def read_file():
    import os
    if os.path.exists('static.txt') and os.stat("static.txt").st_size != 0:
        with open('static.txt', 'r') as json_f:
            return json.load(json_f)
    return None

@periodic_task(run_every=1000)
def get_notifications():
    import copy
    pr_remind_user = {}
    if not read_file():
        logger.info('file ni h ')
        return
    logger.info('abcd {}'.format(json.dumps(read_file())))
    local_user_token_dict = read_file()
    temp_user_token = copy.deepcopy(local_user_token_dict)
    local_user_token_dict = local_user_token_dict.values()
    logger.info("in get notifications: {} aaa".format(read_file()))
    for user_obj in local_user_token_dict:
        access_key = user_obj.get('github_access_key')
        if access_key:
            url = 'https://api.github.com/notifications?access_token={}'.format(access_key)
            response = requests.get(url)
            if response.status_code == 200:
                response_jsons = json.loads(response.content)
                for response_json in response_jsons:
                    # Check if the format of the response remains the same
                    subject_obj = response_json.get('subject')
                    if subject_obj:
                        logger.info("got the subject data")
                        if subject_obj.get('type').lower() == 'pullrequest':
                            # access_key_list.extend()
                            if not pr_remind_user.get(access_key):
                                pr_remind_user[access_key] = [{
                                    'user_id': get_user_id(access_key, temp_user_token),
                                    'url': subject_obj.get('url'),
                                    'subject': subject_obj.get('title')
                                }]
                            else:
                                pr_remind_user[access_key].append({
                                    'user_id': get_user_id(access_key, temp_user_token),
                                    'url': subject_obj.get('url'),
                                    'subject': subject_obj.get('title')
                                })
    logger.info("gng to call remind about pr now {} bbbb".format(json.dumps(pr_remind_user)))
    remind_about_pr(pr_remind_user)


def check_and_update_url(base_url):
    if base_url.startswith('https://'):
        return base_url
    return base_url.replace('http', 'https')