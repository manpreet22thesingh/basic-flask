import requests
import time
import threading
from pyflock import Message, Attachment, Views

base_url = "https://api.github.com/"
access_token = "94da9f1a6c847a0e1c2920d7e012ba9db365210d"
# structure: command username reponame pr#

invalid_command = 'Invalid command!! Please enter a valid command, for help you can always use "gitbot help"'

GET_NOTIFS = "get-notifications"
GET_PUBLIC_EVENTS = "get-activity"
GET_MY_REPOS = "get-all-repos"
GET_REPO_PRs = 'get-prs'
GET_REPO_ISSUES = 'get-issues'
SEARCH_USER = 'search-user'
SEARCH_REPO = 'search-repo'

GITBOT_HELP = "gitbot help"
GITBOT_MESSAGE = "GITBOT HELP\nUsage: command-name <parameters>\n\n" \
                 "Commands: \n1. {0}\n2. {1} username\n3. {2} username\n" \
                 "4. {3} repository-name\n5. {4} username(optional)\n6. {5} username repository-name\n" \
                 "7. {6} username repository-name".format(
    GET_NOTIFS, GET_PUBLIC_EVENTS, SEARCH_USER, SEARCH_REPO, GET_MY_REPOS, GET_REPO_PRs, GET_REPO_ISSUES)

success_codes = [200, 201]


def send_text(text, user_id, flock_client):
    time.sleep(1)
    simple_message = Message(to=user_id, text=text)
    res = flock_client.send_chat(simple_message)


def send_message_to_user(view_data, userId, flock_client):
    views = Views()
    views.add_flockml(view_data)
    if view_data == "<flockml></flockml>":
        send_text("No results found", userId, flock_client)
        return
    attachment = Attachment(views=views)
    send_as_message = Message(to=userId, attachments=[attachment])
    res = flock_client.send_chat(send_as_message)
    thread = threading.Thread(target=send_text, args=("Have more queries? Please Enter your query", userId))
    thread.start()


def goto_github(message, username, userId, flock_client):
    message = ' '.join(message.lower().split())

    if message == GITBOT_HELP:
        view_data = "<flockml>"
        view_data += "<br/>{}<br/><br/>".format(GITBOT_MESSAGE)
        view_data += '</flockml>'
        send_message_to_user(view_data, userId, flock_client)
        return GITBOT_MESSAGE

    command = message.split(" ")[0]

    # TODO handle exceptions wth split
    if command == GET_NOTIFS:
        return requests.get(base_url + "notifications/").text  # TODO make work

    if command == GET_PUBLIC_EVENTS:
        events_list = []
        if len(message.split(" ")) > 1:
            response = requests.get(base_url + "users/{}/received_events/public".format(message.split(" ")[1]))
            if response.status_code in success_codes:
                for data in response.json():
                    events_list.append({"user_name": (data["actor"])["login"],
                                        "repo_name": (data["repo"])["name"],
                                        "url": (data["repo"])["url"]})
                view_data = "<flockml>"
                for r in events_list:
                    view_data += "<a href='{}'>user: {}, repo: {}</a><br/><br/>" \
                        .format(r['url'], r['user_name'], r['repo_name'])
                view_data += '</flockml>'
                send_message_to_user(view_data, userId, flock_client)
                return events_list
        else:
            view_data = "<flockml>"
            view_data += "{}<br/>".format("Atleast one parameter required")
            view_data += '</flockml>'
        thread = threading.Thread(target=send_text, args=(invalid_command, userId, flock_client))
        thread.start()
        return

    if command == SEARCH_USER:
        events_list = []
        if len(message.split(" ")) > 1:
            payload = {"q": message.split(" ")[1]}
            response = requests.get(base_url + "search/users", params=payload)
            if response.status_code in success_codes:
                for data in response.json()["items"]:
                    events_list.append({"user_name": data["login"],
                                        "html_url": data["html_url"],
                                        "url": data["url"]})
                view_data = "<flockml>"
                for r in events_list:
                    view_data += "<a href='{}'>user: {}, repo: {}</a><br/>".format(r['html_url'], r['user_name'],
                                                                                   r['html_url'])
                view_data += '</flockml>'
                send_message_to_user(view_data, userId, flock_client)
                return events_list
        else:
            view_data = "<flockml>"
            view_data += "{}<br/>".format("Atleast one parameter required")
            view_data += '</flockml>'
        thread = threading.Thread(target=send_text, args=(invalid_command, userId, flock_client))
        thread.start()
        return

    if command == SEARCH_REPO:
        events_list = []
        if len(message.split(" ")) > 1:
            payload = {"q": message.split(" ")[1]}
            response = requests.get(base_url + "search/repositories", params=payload)
            if response.status_code in success_codes:
                for data in response.json()["items"]:
                    events_list.append({"user_name": data['owner']['login'],
                                        "html_url": data['owner']["html_url"],
                                        'repo_url': data['html_url']})
                view_data = "<flockml>"
                for r in events_list:
                    view_data += "<a href='{}'>user: {}, repo: {}</a><br/>".format(r['html_url'], r['user_name'],
                                                                                   r['html_url'])
                view_data += '</flockml>'
                send_message_to_user(view_data, userId, flock_client)
                return events_list
        else:
            view_data = "<flockml>"
            view_data += "{}<br/>".format("Atleast one parameter required")
            view_data += '</flockml>'
        thread = threading.Thread(target=send_text, args=(invalid_command, userId, flock_client))
        thread.start()
        return

    if command == GET_MY_REPOS:
        events_list = []
        if len(message.split(" ")) > 1:
            response = requests.get(base_url + "users/{}/repos".format(message.split(" ")[1]))
            if response.status_code in success_codes:
                for data in response.json():
                    events_list.append({"html_url": data['owner']["html_url"],
                                        'repo_url': data['html_url']})
                view_data = "<flockml>"
                for r in events_list:
                    view_data += "<a href='{}'>{}</a><br/>".format(r['html_url'], r['html_url'])
                view_data += '</flockml>'
                send_message_to_user(view_data, userId, flock_client)
                return events_list
        else:
            return requests.get(base_url + "users/{}/repos".format(username)).text

    if command == GET_REPO_PRs:
        events_list = []
        if len(message.split(" ")) > 2:
            response = requests.get(base_url + "repos/{}/{}/pulls".format(message.split(" ")[1], message.split(" ")[2]))
            if response.status_code in success_codes:
                for data in response.json():
                    events_list.append({"html_url": data["html_url"],
                                        "diff_url": data['diff_url'],
                                        'pr_number': data['number'],
                                        'title': data['title'],
                                        'user_name': data['user']['login']})
                view_data = "<flockml>"
                for r in events_list:
                    view_data += "<a href='{}'>{}, repo: {}</a><br/>".format(r['html_url'], r['title'], r['html_url'])
                view_data += '</flockml>'
                send_message_to_user(view_data, userId, flock_client)
                return events_list
        else:
            view_data = "<flockml>"
            view_data += "{}<br/>".format("Atleast two parameters required")
            view_data += '</flockml>'
        thread = threading.Thread(target=send_text, args=(invalid_command, userId, flock_client))
        thread.start()
        return

    if command == GET_REPO_ISSUES:
        events_list = []
        if len(message.split(" ")) > 2:
            response = requests.get(
                base_url + "repos/{}/{}/issues".format(message.split(" ")[1], message.split(" ")[2]))
            if response.status_code in success_codes:
                for data in response.json():
                    events_list.append({"html_url": data["html_url"],
                                        "title": data['title'],
                                        'issue_number': data['number'],
                                        'user_name': data['user']['login'],
                                        'user_profile': data['user']["html_url"]})
                view_data = "<flockml>"
                for r in events_list:
                    view_data += "<a href='{}'>{}</a><br/>".format(r['html_url'], r['title'], r['repo_name'])
                view_data += '</flockml>'
                send_message_to_user(view_data, userId, flock_client)
                return events_list
        else:
            view_data = "<flockml>"
            view_data += "{}<br/>".format("Atleast two parameters required")
            view_data += '</flockml>'
            send_message_to_user(view_data, userId, flock_client)
        thread = threading.Thread(target=send_text, args=(invalid_command, userId, flock_client))
        thread.start()
        return

    thread = threading.Thread(target=send_text, args=(invalid_command, userId, flock_client))
    thread.start()
    return